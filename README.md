The bus reaches as far as the local network. Make sure to switch off the firewall
if you are running on your own machine.

This is the command used to disable the firewall:
```
iptables -I INPUT -s 172.26.89.251 -j ACCEPT
```

A python binding is included
