
#include "felixbus/bus.hpp"

#include "felixbus/elinktable.hpp"
#include "felixbus/felixtable.hpp"
#include "level.hpp"

using namespace felix::bus;

int main(int argc, char *argv[]) {

  Level level(4);

  // FIXME need UUID
  FelixTable felixTable;
  std::string uuid1 = felixTable.addFelix("tcp://192.168.1.8:53467");
  std::string uuid2 = felixTable.addFelix("tcp://192.168.1.12:54467");
  std::string uuid3 = felixTable.addFelix("tcp://192.168.1.20:53468");
  std::string uuid4 = felixTable.addFelix("tcp://192.168.1.44:53468", true, 512, 128*1024);

  ElinkTable elinkTable;
  elinkTable.addElink(1, uuid1);
  elinkTable.addElink(2, uuid2);
  elinkTable.addElink(3, uuid3);
  elinkTable.addElink(4, uuid4);
  elinkTable.addElink(5, uuid4);

  Bus bus;

  bus.setVerbose(true);
  bus.setZyreVerbose(true);

  bus.publish("FELIX", felixTable);
  bus.publish("ELINKS", elinkTable);

  bus.connect();

  sleep(5);

  if (!zsys_interrupted) {
    bus.publish("LEVEL", level);
    sleep(5);
  }

  if (!zsys_interrupted) {
    level.set(7);
    bus.publish("LEVEL", level);
    sleep(5);
  }

  if (!zsys_interrupted) {
    std::string uuid = felixTable.addFelix("tcp://10.0.0.1:128");
    elinkTable.addElink(1, uuid);
    bus.publish("FELIX", felixTable);
    bus.publish("ELINKS", elinkTable);
  }

  sleep(60);

  bus.disconnect();

  return 0;
}
