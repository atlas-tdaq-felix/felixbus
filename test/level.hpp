#ifndef FELIXBUS_LEVEL_HPP
#define FELIXBUS_LEVEL_HPP

#include <map>
#include <string>
#include <sstream>
#include <iostream>

#include "felixbus/serializable.hpp"

namespace felix
{
namespace bus
{
class Level : public Serializable {

public:
  Level(int level = -1) : level(level) {};
  virtual ~Level() {};

  int get() {
    return level;
  }

  void set(int level) {
    this->level = level;
  }

public:
  virtual std::string serialize() {
    return std::to_string(level);
  }

  virtual void deserialize(std::string peerId, std::string s) {
    this->peerId = peerId;
    level = std::stoi(s);
  }

  virtual void removePeer(std::string peerId) {
    if (this->peerId == peerId) {
      this->peerId = "";
      level = -1;
    }
  }

  virtual void updated() {
    std::cout << "Level Updated: " << get() << std::endl;
  }

private:
  int level;
  std::string peerId = "";
};

}
}

#endif
