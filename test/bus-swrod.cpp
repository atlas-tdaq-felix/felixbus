#include "felixbus/bus.hpp"

#include "felixbus/elinktable.hpp"
#include "felixbus/felixtable.hpp"
#include "level.hpp"

using namespace felix::bus;

int main(int argc, char *argv[]) {

  Level level;
  FelixTable felixTable;
  ElinkTable elinkTable;

  std::cout << "Creating Bus" << std::endl;
  Bus bus;

  std::cout << "Setting verbose" << std::endl;
  bus.setVerbose(true);
  bus.setZyreVerbose(true);

  std::cout << "Subscribing FELIX and ELINKS" << std::endl;
  bus.subscribe("FELIX", &felixTable);
  bus.subscribe("ELINKS", &elinkTable);

  std::cout << "Connect Bus" << std::endl;
  bus.connect();

  std::cout << "Subscribing LEVEL" << std::endl;
  bus.subscribe("LEVEL", &level);

  // sleep(60);

  std::cout << "Reading..." << std::endl;
  std::string uuid = elinkTable.getFelixId(1);
  std::cout << "UUID 1: " << uuid << std::endl;
  std::cout << "Elink 1: " << felixTable.getAddress(uuid) << std::endl;
  std::cout << "Level: " << level.get() << std::endl;

  // direct call
  std::cout << "Direct Call" << std::endl;
  std::cout << elinkTable.getFelixId(1) << std::endl;

  // timeout call
  std::cout << "Timeout Call"  << std::endl;
  std::cout << elinkTable.getFelixId(1, 5000) << std::endl;

  // blocked call
  std::cout << "Blocked Call"  << std::endl;
  std::cout << elinkTable.getFelixId(1, 0) << std::endl;

  // other info
  std::cout << "Other info"  << std::endl;
  std::string uuid5 = elinkTable.getFelixId(5);
  std::cout << "Elink 5: " << felixTable.getAddress(uuid5) << std::endl;
  std::cout << "Elink 5 Unbuffered: " << felixTable.isUnbuffered(uuid5) << std::endl;
  std::cout << "Elink 5 Netio Pages: " << felixTable.getNetioPages(uuid5) << std::endl;
  std::cout << "Elink 5 Netio PageSize: " << felixTable.getNetioPageSize(uuid5) << std::endl;

  // done
  std::cout << "Finished" << std::endl;
  return 0;
}
