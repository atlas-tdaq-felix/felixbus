#ifndef FELIXBUS_BUS_HPP
#define FELIXBUS_BUS_HPP

#include <string>
#include <map>
#include <exception>
#include <thread>
#include <mutex>

#include "czmq.h"
#include "zyre.h"

#include "serializable.hpp"

#define PREFIX "flx-bus:"

namespace felix {
  namespace bus {

    typedef std::map<std::string, Serializable*> DataMap;

    /**
     * Return ip address of given interface, (interface,"") if not found.
     * for the "default" interface the first interface is returned but never localHost (127.0.0.1).
     */
    inline std::pair<std::string, std::string> getIpAddress(std::string interface = "default") {
      std::pair<std::string, std::string> defaultIpAddress(interface,"");
      std::string localHost = "127.0.0.1";

      // get list of interfaces
      struct ifaddrs * ifAddrStruct=NULL;
      getifaddrs(&ifAddrStruct);

      for (struct ifaddrs* ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr) { // check connected
          if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
            if ((interface == ifa->ifa_name) ||
                ((interface == "default") && (defaultIpAddress.second == ""))) {
              void* tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
              char addressBuffer[INET_ADDRSTRLEN];
              inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);

              if (interface == ifa->ifa_name) {
                defaultIpAddress = std::make_pair(interface, addressBuffer);
                freeifaddrs(ifAddrStruct);
                return defaultIpAddress;
              } else if (localHost != addressBuffer) {
                defaultIpAddress = std::make_pair(ifa->ifa_name, addressBuffer);
              }
            }
          } else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
            // ignore for now
    //          tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
    //          char addressBuffer[INET6_ADDRSTRLEN];
    //          inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
    //          printf("%s IP6 Address %s\n", ifa->ifa_name, addressBuffer);
          }
        }
      }

      freeifaddrs(ifAddrStruct);
      return defaultIpAddress;
    }


    class Bus {

      public:
        Bus(std::string groupName = "FELIX", std::string interface = "ZSYS_INTERFACE", bool verbose = false, bool zyreVerbose = false);
        ~Bus();

        void connect();
        void disconnect();

        void publish(std::string key, Serializable& data);
        void subscribe(std::string key, Serializable* data);

        void setGroupName(std::string i) {
          groupName = i;
        }

        void setInterface(std::string i) {
          interface = i;
        }

        void setVerbose(bool b) {
          verbose = b;
        }

        void setZyreVerbose(bool b) {
          zyreVerbose = b;
        }

      private:

      // name: BusMutex
      // description: This is kind of a hack - necessary to prevent a race condition.
      // This class is a singleton by internal implementation, not by interface. This
      // separates the concerns of its function and its form, meaning client programs
      // do not need to care.

      class BusMutex {
      private:
        struct Instance {

          std::mutex mtx;

          std::unique_lock<std::mutex> acquire_lock() {
            return std::unique_lock<std::mutex>(mtx);
          }
        };

        static Instance& getInstance() {
          static Instance instance {};
          return instance;
        }

      public:
        std::unique_lock<std::mutex> acquire_lock() {
          return getInstance().acquire_lock();
        }
      };

      static void messageHandler(felix::bus::Bus* bus,
                                 std::unique_lock<std::mutex> zyre_lock) {
          try {
            bus->handleMessages(std::move(zyre_lock));
          } catch (std::exception& e) {
            zsys_error("%s Uncaught exception in felix bus thread: %s", PREFIX, e.what());
            throw;
          }
        }

        void handleMessages(std::unique_lock<std::mutex>);
        bool handleCommand(zyre_t *threadNode, std::string peerId, std::string group, std::string message);
        void removePeer(std::string peerId);

        std::string groupName = "FELIX";
        std::string interface = "ZSYS_INTERFACE";
        bool verbose = false;
        bool zyreVerbose = false;

        zyre_t *node = NULL;

        std::thread* messageThread = NULL;
        std::string threadPeerId = "";

        DataMap publishData;
        DataMap subscribeData;

        bool connected = false;
    };
  }
}

#endif
