#ifndef FELIXBUS_ELINKTABLE_HPP
#define FELIXBUS_ELINKTABLE_HPP

#include <algorithm>
#include <chrono>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <thread>

#include "nlohmann/json.hpp"

#include "serializable.hpp"

using json = nlohmann::json;

namespace felix
{
namespace bus
{

  struct ElinkEntry {
    std::string felixId;
    std::string peerId;
  };

  typedef std::map<ulong, ElinkEntry> ElinkMap;

class ElinkTable : public Serializable {

public:
  ElinkTable() {};
  virtual ~ElinkTable() {};

  void addElink(ulong elink, std::string felixId, std::string peerId = "") {
    ElinkEntry e;
    e.felixId = felixId;
    e.peerId = peerId;

    table[elink] = e;
  }

  void removeElink(ulong elink) {
    table.erase(elink);
  }

  // w/o timeout -> immediate, 0 timeout -> blocking, x timeout standard behaviour
  std::string getFelixId(ulong elink, int timeoutms = -1) {
    if (timeoutms < 0) {
      // std::cout << "getFelixId direct" << std::endl;
      return findFelixId(elink);
    }

    int timeout = 0;
    uint span = timeoutms > 0 ? std::min(timeoutms, 250) : 250;
    std::string fid;
    while (((fid = findFelixId(elink)) == "") && ((timeoutms == 0) || (timeout <= timeoutms))) {
      std::this_thread::sleep_for(std::chrono::milliseconds(span));
      timeout += span;
    }
    // if (timeoutms < 0) {
    //   std::cout << "getFelixId timed out" << std::endl;
    // }
    return fid;
  }

  // w/o timeout -> immediate, 0 timeout -> blocking, x timeout standard behaviour
  std::string getPeerId(ulong elink, int timeoutms = -1) {
    if (timeoutms < 0) {
      return findPeerId(elink);
    }

    int timeout = 0;
    uint span = timeoutms > 0 ? std::min(timeoutms, 250) : 250;
    std::string fid;
    while (((fid = findPeerId(elink)) == "") && ((timeoutms == 0) || (timeout <= timeoutms))) {
      std::this_thread::sleep_for(std::chrono::milliseconds(span));
      timeout += span;
    }
    return fid;
  }

  std::set<ulong> getLinks() {
    std::set<ulong> links;
    for (ElinkMap::iterator it = table.begin(); it != table.end(); it++) {
      links.insert(it->first);
    }
    return links;
  }

  void print() {
    if (table.size() > 100) {
      std::cout << "# of links: " << table.size() << std::endl;
    } else {
      std::cout << "ElinkTable::print(): table.size()=" << table.size() << std::endl;
      std::cout << "PeerId                            " <<
                   "FelixInstanceId                   " <<
                   "FID or ElinkId" << std::endl;
      for (ElinkMap::iterator it = table.begin(); it != table.end(); it++) {
        ulong elink = it->first;
        ElinkEntry e = it->second;
        std::cout << e.peerId << "  " << e.felixId << "  " << elink << std::endl;
      }
    }
  }

public:
  virtual std::string serialize() {
    std::ostringstream oss;

    ElinkMap::iterator it = table.begin();
    while(it != table.end()) {
      ulong elink = it->first;
      ElinkEntry e = it->second;
      oss << elink << delimiter << e.felixId;
      it++;
      if (it != table.end()) {
        oss << delimiter;
      }
    }

    return oss.str();
  }

  virtual void deserialize(std::string peerId, std::string s) {

    std::istringstream is(s);
    do {
      std::string elink;
      std::getline(is, elink, delimiter);

      std::string felixId;
      std::getline(is, felixId, delimiter);

      // std::cout << "Elink: '" << elink << "', FelixId: '" << felixId << "' in '" << s << "' for " << peerId << std::endl;

      if ((elink != "") && (felixId != "")) {
        addElink(std::stoul(elink), felixId, peerId);
      }
    } while (!is.eof());
  }

  virtual void removePeer(std::string peerId) {
    ElinkMap::iterator it = table.begin();
    while(it != table.end()) {
      if (it->second.peerId == peerId) {
        it = table.erase(it);
      } else {
        it++;
      }
    }
  }

  virtual void updated() {
    // no-op
  }

  json toJson() {
    json a;
    ElinkMap::iterator it = table.begin();
    while(it != table.end()) {
      ulong elink = it->first;
      ElinkEntry e = it->second;
      json o;
      o["elink"] = elink;
      o["felixId"] = e.felixId;
      a.push_back(o);
      it++;
    }
    return a;
  }

private:
  char delimiter = ' ';
  ElinkMap table;

  // non-blocking
  std::string findFelixId(ulong elink) {
    ElinkMap::iterator it = table.find(elink);
    if (it != table.end()) {
      return it->second.felixId;
    }
    return "";
  }

  // non-blocking
  std::string findPeerId(ulong elink) {
    ElinkMap::iterator it = table.find(elink);
    if (it != table.end()) {
      return it->second.peerId;
    }
    return "";
  }

  // blocking
  std::string getPeerIdBlocking(ulong elink) {
    std::string pid;
    while ((pid = findPeerId(elink)) == "") {
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    return pid;
  }
};

}
}

#endif
