#ifndef FELIXBUS_FELIXTABLE_HPP
#define FELIXBUS_FELIXTABLE_HPP

#include <map>
#include <string>
#include <sstream>
#include <iostream>

#include "nlohmann/json.hpp"

#include "serializable.hpp"

using json = nlohmann::json;

#define NETIO_PAGES 256
#define NETIO_PAGESIZE 64*1024

namespace felix
{
namespace bus
{

  struct FelixEntry {
    std::string address;    // ex: tcp://192.168.1.4:56747
    std::string peerId;
    bool unbuffered;
    bool pubsub;
    uint netio_pages;
    uint netio_pagesize;
  };

  typedef std::map<std::string, FelixEntry> FelixMap;

class FelixTable : public Serializable {

public:
  FelixTable() {};
  virtual ~FelixTable() {};

  // std::string addFelix(std::string address, bool noFelixId = false, std::string peerId = "") {
  //   zuuid_t *uuid = zuuid_new();
  //   std::string felixId = noFelixId ? "" : zuuid_str(uuid);
  //   zuuid_destroy (&uuid);
  //   addFelix(felixId, address, peerId);
  //   return felixId;
  // }

  std::string addFelix(std::string address, bool unbuffered = false, bool pubsub = true, uint netio_pages = NETIO_PAGES, uint netio_pagesize = NETIO_PAGESIZE, std::string peerId = "") {
    zuuid_t *uuid = zuuid_new();
    std::string felixId = zuuid_str(uuid);
    zuuid_destroy (&uuid);
    _addFelix(felixId, address, unbuffered, pubsub, netio_pages, netio_pagesize, peerId);
    return felixId;
  }

  std::string getAddress(std::string felixId) {
    FelixMap::iterator it = table.find(felixId);
    if (it != table.end()) {
      return it->second.address;
    }
    return "";
  }

  bool isUnbuffered(std::string felixId) {
    FelixMap::iterator it = table.find(felixId);
    if (it != table.end()) {
      return it->second.unbuffered;
    }
    return false;
  }

  bool isPubSub(std::string felixId) {
    FelixMap::iterator it = table.find(felixId);
    if (it != table.end()) {
      return it->second.pubsub;
    }
    return false;
  }

  uint getNetioPages(std::string felixId) {
    FelixMap::iterator it = table.find(felixId);
    if (it != table.end()) {
      return it->second.netio_pages;
    }
    return NETIO_PAGES;
  }

  uint getNetioPageSize(std::string felixId) {
    FelixMap::iterator it = table.find(felixId);
    if (it != table.end()) {
      return it->second.netio_pagesize;
    }
    return NETIO_PAGESIZE;
  }

  std::string getPeerId(std::string felixId) {
    FelixMap::iterator it = table.find(felixId);
    if (it != table.end()) {
      return it->second.peerId;
    }
    return "";
  }

  void print() {
    std::cout << "FelixTable::print(): table.size()=" << table.size() << std::endl;
    std::cout << "PeerId                            " <<
                 "FelixInstanceID                   " <<
                 "Address  " <<
                 "PubSub  " <<
                 "Unbuffered  " <<
                 "PageSize  " <<
                 "Pages  " << std::endl;
    for (FelixMap::iterator it = table.begin(); it != table.end(); it++) {
      std::string felixId = it->first;
      FelixEntry e = it->second;
      std::cout << e.peerId << "  " << (felixId != "" ? felixId : std::string(ZUUID_STR_LEN, ' '))
                << "  " << e.address
                << "  " << e.pubsub
                << "  " << e.unbuffered
                << "  " << e.netio_pagesize
                << "  " << e.netio_pages
                << std::endl;
    }
  }

public:
  virtual std::string serialize() {
    std::ostringstream oss;

    FelixMap::iterator it = table.begin();
    while(it != table.end()) {
      std::string felixId = it->first;
      FelixEntry e = it->second;
      oss << felixId << delimiter << e.address << delimiter << e.unbuffered << delimiter << e.pubsub << delimiter << e.netio_pages << delimiter << e.netio_pagesize;
      it++;
      if (it != table.end()) {
        oss << delimiter;
      }
    }

    return oss.str();
  }

  virtual void deserialize(std::string peerId, std::string s) {

    // std::cout << "RAW: '" << s << "'" << std::endl;

    std::istringstream is(s);
    do {
      std::string felixId;
      std::getline(is, felixId, delimiter);

      std::string address;
      std::getline(is, address, delimiter);

      std::string unbuffered_str = "";
      std::getline(is, unbuffered_str, delimiter);
      bool unbuffered = unbuffered_str == "" ? false : std::stoi(unbuffered_str);

      std::string pubsub_str = "";
      std::getline(is, pubsub_str, delimiter);
      bool pubsub = pubsub_str == "" ? false : std::stoi(pubsub_str);

      std::string netio_pages_str = "";
      std::getline(is, netio_pages_str, delimiter);
      uint netio_pages = netio_pages_str == "" ? NETIO_PAGES : std::stoul(netio_pages_str);

      std::string netio_pagesize_str = "";
      std::getline(is, netio_pagesize_str, delimiter);
      uint netio_pagesize = netio_pagesize_str == "" ? NETIO_PAGESIZE : std::stoul(netio_pagesize_str);

      _addFelix(felixId, address, unbuffered, pubsub, netio_pages, netio_pagesize, peerId);
    } while (!is.eof());
  }

  virtual void removePeer(std::string peerId) {
    FelixMap::iterator it = table.begin();
    while(it != table.end()) {
      if (it->second.peerId == peerId) {
        it = table.erase(it);
      } else {
        it++;
      }
    }
  }

  virtual void updated() {
    // no-op
  }

  json toJson() {
    json a;

    FelixMap::iterator it = table.begin();
    while(it != table.end()) {
      std::string felixId = it->first;
      FelixEntry e = it->second;
      json o;
      o["felixId"] = felixId;
      o["peerId"] = e.peerId;
      o["address"] = e.address;
      o["unbuffered"] = e.unbuffered;
      o["pubsub"] = e.pubsub;
      o["netio_pages"] = e.netio_pages;
      o["netio_pagesize"] = e.netio_pagesize;
      a.push_back(o);
      it++;
    }
    return a;
  }

private:
  void _addFelix(std::string felixId, std::string address, bool unbuffered, bool pubsub, uint netio_pages, uint netio_pagesize, std::string peerId) {
    FelixEntry e;
    e.address = address;
    e.unbuffered = unbuffered;
    e.pubsub = pubsub;
    e.netio_pages = netio_pages;
    e.netio_pagesize = netio_pagesize;
    e.peerId = peerId;

    table[felixId] = e;
  }

  char delimiter = ' ';
  FelixMap table;
};

}
}

#endif
