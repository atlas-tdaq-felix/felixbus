#ifndef FELIXBUS_SERIALIZABLE_HPP
#define FELIXBUS_SERIALIZABLE_HPP

namespace felix
{
namespace bus
{

class Serializable {

public:
  Serializable() {};
  virtual ~Serializable() {};

protected:
  virtual std::string serialize() = 0;
  virtual void deserialize(std::string peerId, std::string s) = 0;
  virtual void removePeer(std::string peerId) = 0;
  virtual void updated() { /* no-op */ };

  friend class Bus;
};

}
}

#endif
