#ifndef FELIXBUS_PROCESSTABLE_HPP
#define FELIXBUS_PROCESSTABLE_HPP

#include <algorithm>
#include <chrono>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <thread>

#include <sys/types.h>
#include <unistd.h>

#include "nlohmann/json.hpp"

#include "serializable.hpp"

using json = nlohmann::json;

namespace felix
{
namespace bus
{

  struct ProcessEntry {
    std::string user_hostname_pid;
    std::string peerId;
  };

  typedef std::map<std::string, ProcessEntry> ProcessMap;

class ProcessTable : public Serializable {

public:
  ProcessTable() {};
  virtual ~ProcessTable() {};

  void addProcess(std::string felixId, std::string user_hostname_pid = "", std::string peerId = "") {
    if (user_hostname_pid == "") {
      char* user_name;
      uid_t uid = geteuid ();
      struct passwd *pw = getpwuid (uid);
      if (pw) {
        user_name = pw->pw_name;
      } else {
        user_name = NULL;
      }
      char host_name[256];
      gethostname(host_name, 256);
      user_hostname_pid = user_name ? std::string(user_name) + "@" : "unknown@";
      user_hostname_pid += std::string(host_name) + ":" + std::to_string(getpid());
    }

    ProcessEntry p;
    p.user_hostname_pid = user_hostname_pid;
    p.peerId = peerId;
    table[felixId] = p;
  }

  void removeProcess(std::string felixId) {
    table.erase(felixId);
  }

  std::string getUserHostnamePid(std::string felixId) {
    ProcessMap::iterator it = table.find(felixId);
    if (it != table.end()) {
      return it->second.user_hostname_pid;
    }
    return "";
  }

  void print() {
    if (table.size() > 100) {
      std::cout << "# of links: " << table.size() << std::endl;
    } else {
      std::cout << "ProcessTable::print(): table.size()=" << table.size() << std::endl;
      std::cout << "PeerId                            " <<
                   "FelixInstanceId                   " <<
                   "user@hostname:pid" << std::endl;
      for (ProcessMap::iterator it = table.begin(); it != table.end(); it++) {
        std::string felixId = it->first;
        ProcessEntry p = it->second;
        std::cout << p.peerId << "  " << felixId << "  " << p.user_hostname_pid << std::endl;
      }
    }
  }

public:
  virtual std::string serialize() {
    std::ostringstream oss;

    ProcessMap::iterator it = table.begin();
    while(it != table.end()) {
      std::string felixId = it->first;
      ProcessEntry p = it->second;
      oss << felixId << delimiter << p.user_hostname_pid;
      it++;
      if (it != table.end()) {
        oss << delimiter;
      }
    }

    return oss.str();
  }

  virtual void deserialize(std::string peerId, std::string s) {

    std::istringstream is(s);
    do {
      std::string felixId;
      std::getline(is, felixId, delimiter);

      std::string user_hostname_pid;
      std::getline(is, user_hostname_pid, delimiter);

      if (user_hostname_pid != "") {
        addProcess(felixId, user_hostname_pid, peerId);
      }
    } while (!is.eof());
  }

  virtual void removePeer(std::string peerId) {
    ProcessMap::iterator it = table.begin();
    while(it != table.end()) {
      if (it->second.peerId == peerId) {
        it = table.erase(it);
      } else {
        it++;
      }
    }
  }

  virtual void updated() {
    // no-op
  }

  json toJson() {
    json a;
    ProcessMap::iterator it = table.begin();
    while(it != table.end()) {
      std::string felixId = it->first;
      ProcessEntry p = it->second;
      json o;
      o["peerId"] = p.peerId;
      o["felixId"] = felixId;
      o["user_hostname_pid"] = p.user_hostname_pid;
      a.push_back(o);
      it++;
    }
    return a;
  }

private:
  char delimiter = ' ';
  ProcessMap table;
};

}
}

#endif
