#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <array>
#include <iostream>
#include <memory>
#include <vector>

namespace py = pybind11;

#include "felixbus/bus.hpp"
#include "felixbus/serializable.hpp"
#include "felixbus/elinktable.hpp"
#include "felixbus/felixtable.hpp"
#include "felixbus/processtable.hpp"

using namespace felix::bus;

// struct flxcard_py {
//     enum LOCK {
//         NONE = LOCK_NONE,
//         DMA0 = LOCK_DMA0,
//         DMA1 = LOCK_DMA1,
//         I2C = LOCK_I2C,
//         FLASH = LOCK_FLASH,
//         ELINK = LOCK_ELINK,
//         ALL = LOCK_ALL
//     };
// };

PYBIND11_MODULE(libfelixbus_py, m) {
    m.doc() = "Python bindings for FelixBus.";

    py::class_<Serializable, std::shared_ptr<Serializable>> serializable(m, "Serializable");


    py::class_<ProcessTable, std::shared_ptr<ProcessTable>> process_table(m, "ProcessTable", serializable);

    // ProcessTable();
    process_table.def(py::init<>())
        // void addProcess(std::string felixId, std::string user_hostname_pid = "", std::string peerId = "")
        .def("addProcess", &ProcessTable::addProcess, py::arg("felixId"), py::arg("user_hostname_pid") = "", py::arg("peerId") = "")
        // void print();
        .def("print_table", &ProcessTable::print)
        // void removeProcess(std::string felixId)
        .def("removeProcess", &ProcessTable::removeProcess, py::arg("felixId"))
        // std::string getUserHostnamePid(std::string felixId)
        .def("getUserHostnamePid", &ProcessTable::getUserHostnamePid, py::arg("felixId"))
    ;


    py::class_<FelixTable, std::shared_ptr<FelixTable>> felix_table(m, "FelixTable", serializable);

    // FelixTable();
    felix_table.def(py::init<>())
        // std::string addFelix(std::string address, bool unbuffered = false, bool pubsub = true, uint netio_pages = NETIO_PAGES, uint netio_pagesize = NETIO_PAGESIZE, std::string peerId = "")
        // std::string addFelix(std::string address, bool unbuffered = false, uint netio_pages = NETIO_PAGES, uint netio_pagesize = NETIO_PAGESIZE, std::string peerId = "") {
        .def("addFelix", (std::string (FelixTable::*)(std::string, bool, bool, uint, uint, std::string)) &FelixTable::addFelix,
            py::arg("address"), py::arg("unbuffered") = false, py::arg("pubsub") = true, py::arg("netio_pages") = 256, py::arg("netio_pagesize") = 64*1024, py::arg("peerId") = "")
        // std::string getAddress(std::string felixId);
        .def("getAddress", &FelixTable::getAddress, py::arg("felixId"))
        // bool isUnbuffered(std::string felixId);
        .def("isUnbuffered", &FelixTable::isUnbuffered, py::arg("felixId"))
        // bool isPubSub(std::string felixId)
        .def("isPubSub", &FelixTable::isPubSub, py::arg("felixId"))
        // uint getNetioPages(std::string felixId);
        .def("getNetioPages", &FelixTable::getNetioPages, py::arg("felixId"))
        // uint getNetioPageSize(std::string felixId);
        .def("getNetioPageSize", &FelixTable::getNetioPageSize, py::arg("felixId"))
        // std::string getPeerId(std::string felixId);
        .def("getPeerId", &FelixTable::getPeerId, py::arg("felixId"))
        // void print();
        .def("print_table", &FelixTable::print)
    ;

    py::class_<ElinkTable, std::shared_ptr<ElinkTable>> elink_table(m, "ElinkTable", serializable);

    // ElinkTable();
    elink_table.def(py::init<>())
        // void addElink(uint elink, std::string felixId, std::string peerId = "");
        .def("addElink", &ElinkTable::addElink,
            py::arg("elink"), py::arg("felixId"), py::arg("peerId") = "")
        // std::string getFelixId(uint elink);
        .def("getFelixId", &ElinkTable::getFelixId, py::arg("elink"), py::arg("timeoutms") = -1)
        // std::string getPeerId(uint elink);
        .def("getPeerId", &ElinkTable::getPeerId, py::arg("elink"), py::arg("timeoutms") = -1)
        // std::set<uint> getLinks()
        .def("getLinks", &ElinkTable::getLinks)
        // void print();
        .def("print_table", &ElinkTable::print)
        // void removeElink(uint elink)
        .def("removeElink", &ElinkTable::removeElink, py::arg("elink"))
    ;


    py::class_<Bus, std::shared_ptr<Bus>> bus(m, "Bus");

    // Bus(std::string groupName = "FELIX", std::string interface = "ZSYS_INTERFACE", bool verbose = false, bool zyreVerbose = false);
    bus.def(py::init<const std::string &, const std::string &, bool, bool>(),
            py::arg("group_name") = "FELIX", py::arg("interface") = "ZSYS_INTERFACE", py::arg("verbose") = false, py::arg("zyre_verbose") = false)

        // void connect();
        .def("connect", &Bus::connect)
        // void disconnect();
        .def("disconnect", &Bus::disconnect)

        // void publish(std::string key, Serializable& data);
        .def("publish", &Bus::publish, py::arg("key"), py::arg("data"))
        // void subscribe(std::string key, Serializable* data);
        .def("subscribe", &Bus::subscribe, py::arg("key"), py::arg("data"))

        // void setGroupName(std::string i);
        .def("setGroupName", &Bus::setGroupName, py::arg("name"))
        // void setInterface(std::string i);
        .def("setInterface", &Bus::setInterface, py::arg("name"))
        // void setVerbose(bool b);
        .def("setVerbose", &Bus::setVerbose, py::arg("verbose"))
        // void setZyreVerbose(bool b);
        .def("setZyreVerbose", &Bus::setZyreVerbose, py::arg("zyre_verbose"))

        // std::pair<std::string, std::string> getIpAddress(std::string interface = "default")
        .def_static("getIpAddress", &getIpAddress, py::arg("interface") = "default")
    ;
}
