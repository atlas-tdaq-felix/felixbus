#include <set>
#include <string>
#include <sstream>
#include <iostream>
#include <utility>

#include "felixbus/bus.hpp"


felix::bus::Bus::Bus(std::string groupName, std::string interface, bool verbose, bool zyreVerbose) :
    groupName(groupName), interface(interface), verbose(verbose), zyreVerbose(zyreVerbose)
{
  zsys_init();
  zsys_handler_reset();
}

felix::bus::Bus::~Bus() {
  disconnect();
}

void felix::bus::Bus::connect() {
  if (connected) {
    return;
  }

  if (verbose) {
    zsys_info("%s Connect", PREFIX);
  }
  node = zyre_new (NULL);
  if (zyreVerbose) {
    zyre_set_verbose (node);
  }

  auto zyre_lock = BusMutex().acquire_lock();
  // NOTE: this way interface can be set by env: ZSYS_INTERFACE
  if (interface != "ZSYS_INTERFACE") {
    std::pair<std::string, std::string> address = getIpAddress(interface);
    if (address.second == "") {
      zsys_error("%s Cannot find IP address for interface: %s", PREFIX, address.first.c_str());
      exit(1);
    }

    zyre_set_interface(node, address.first.c_str());
    if (verbose) {
      zsys_info("%s Interface: %s", PREFIX, address.first.c_str());
    }
  }

  zyre_start (node);
  if (verbose) {
    zsys_info("%s PeerId Node: %6.6s", PREFIX, zyre_uuid(node));
  }
//  zyre_join(node, groupName.c_str());

  messageThread = new std::thread(messageHandler, this, std::move(zyre_lock));

  connected = true;
}

void felix::bus::Bus::disconnect() {
  if (!connected) {
    return;
  }

  if (verbose) {
    zsys_info("%s Disconnect", PREFIX);
  }

  if (messageThread) {
    if (node) {
      zsys_info("%s Telling message thread to finish", PREFIX);
      zyre_whispers(node, threadPeerId.c_str(), "QUIT");
    }

    if (messageThread->joinable()) {
      if (verbose) {
        zsys_info("%s Waiting for message thread to finish...", PREFIX);
      }

      messageThread->join();
      if (verbose) {
        zsys_info("%s Message thread finished", PREFIX);
      }
    }

    delete messageThread;
  }

  if (node) {
    if (verbose) {
      zsys_info("%s Stopping zyre...", PREFIX);
    }
// NOTE: does not work with ctrl-c
//      zyre_stop(node);

    zyre_destroy(&node);
    node = NULL;
    if (verbose) {
      zsys_info("%s Zyre stopped", PREFIX);
    }
  }

  connected = false;
}

void felix::bus::Bus::publish(std::string key, Serializable& data) {
  if (verbose) {
    zsys_info("%s Publish Data for %s", PREFIX, key.c_str());
  }

  publishData[key] = &data;

  // inform all the group
  if (node) {
    zyre_shouts(node, groupName.c_str(), "UPDATE %s %s", key.c_str(), data.serialize().c_str());
  }
}

void felix::bus::Bus::subscribe(std::string key, Serializable* data) {
  if (verbose) {
    zsys_info("%s Subscribe Data for %s", PREFIX, key.c_str());
  }
  subscribeData[key] = data;

  // already connected, we need info from the full group
  if (node) {
    zyre_shouts(node, groupName.c_str(), "REQUEST %s", key.c_str());
  }
}

void felix::bus::Bus::handleMessages(std::unique_lock<std::mutex> zyre_lock) {
  std::set<std::string> groupSet;

  zyre_t *threadNode = zyre_new (NULL);
  if (zyreVerbose) {
    zyre_set_verbose (threadNode);
  }
  zyre_start (threadNode);
  zyre_lock.unlock();
  threadPeerId = zyre_uuid (threadNode);
  if (verbose) {
    zsys_info("%s PeerId ThreadNode: %6.6s", PREFIX, threadPeerId.c_str());
  }
  zyre_join(threadNode, groupName.c_str());

  bool stop = false;
  zmsg_t *msg;
  while (!stop && (msg  = zyre_recv (threadNode))) {
    char *command = zmsg_popstr (msg);
    char *peerId = zmsg_popstr (msg);
    char *name = zmsg_popstr (msg);

    if (streq (command, "ENTER")) {
      // ENTER
      // no-op
    } else if (streq (command, "EVASIVE")) {
      // EVASIVE
      // no-op
    } else if (streq (command, "EXIT")) {
      // EXIT
      removePeer(peerId);
      groupSet.erase(peerId);

    } else if (streq (command, "JOIN")) {
      // JOIN
      char *group = zmsg_popstr (msg);
      if (group == groupName) {
        if (verbose) {
          zsys_info("%s Adding Peer %6.6s", PREFIX, peerId);
        }

        // add to groupSet
        groupSet.insert(peerId);

        // request all current data
        for (DataMap::iterator it = subscribeData.begin(); it != subscribeData.end(); it++) {
          std::string key = it->first;
          if (verbose) {
            zsys_info("%s Requesting data for %s by %6.6s", PREFIX, key.c_str(), peerId);
          }
          Serializable* data = it->second;
          if (data) {
            zyre_shouts(threadNode, groupName.c_str(), "REQUEST %s", key.c_str());
          }
        }
      }

      zstr_free (&group);

    } else if (streq (command, "LEAVE")) {
      // LEAVE
      char *group = zmsg_popstr (msg);
      // remove from groupSet, modify table
      if (group == groupName) {
        removePeer(peerId);
        groupSet.erase(peerId);
      }
      zstr_free (&group);

    } else if (streq (command, "WHISPER")) {
      // WHISPER
      char *message = zmsg_popstr (msg);

      std::string group = groupSet.find(peerId) != groupSet.end() ? groupName : "";

      stop = handleCommand(threadNode, peerId, group, message);

      zstr_free (&message);
    } else if (streq (command, "SHOUT")) {
      // SHOUT
      char *group = zmsg_popstr (msg);

      char *message = zmsg_popstr (msg);

      stop = handleCommand(threadNode, peerId, group, message);

      zstr_free (&message);
      zstr_free (&group);

    } else {
      zsys_error("%s ERROR Unknown command: %s", PREFIX, command);
    }

    zstr_free (&peerId);
    zstr_free (&name);
    zstr_free (&command);
    zmsg_destroy (&msg);
  }

//  zyre_leave(threadNode, groupName.c_str());
// NOTE: may not handle ctrl-c
//  zyre_stop(threadNode);

  threadPeerId = "";
  zyre_destroy(&threadNode);
}

bool felix::bus::Bus::handleCommand(zyre_t *threadNode, std::string peerId, std::string group, std::string message) {
  std::istringstream is(message);

  std::string cmd;
  std::getline(is, cmd, ' ');

  std::string params;
  std::getline(is, params);

  if (group == groupName) {
    if (cmd == "UPDATE") {
      std::istringstream is(params);

      std::string key;
      std::getline(is, key, ' ');

      std::string params;
      std::getline(is, params);

      Serializable* data = subscribeData[key];
      if (data) {
        if (verbose) {
          zsys_info("%s UPDATE from Peer %6.6s for %s", PREFIX, peerId.c_str(), key.c_str());
        }

  //        std::cout << params << std::endl;
        data->deserialize(peerId, params);
        data->updated();
      }
    } else if (cmd == "REQUEST") {
      std::string key = params;

      Serializable* data = publishData[key];
      if (data) {
        if (verbose) {
          zsys_info("%s REQUEST from Peer %6.6s for %s, whispering UPDATE", PREFIX, peerId.c_str(), key.c_str());
        }
        zyre_whispers(threadNode, peerId.c_str(), "UPDATE %s %s", key.c_str(), data->serialize().c_str());
      }
    } else {
      zsys_error("%s Unknown Group Cmd %s from %6.6s", PREFIX, cmd.c_str(), peerId.c_str());
    }
  }

  if (cmd == "QUIT") {
    if (verbose) {
      zsys_info("%s QUIT: Shutting down message handling", PREFIX);
    }
    return true;
  }

  return false;
}

void felix::bus::Bus::removePeer(std::string peerId) {
  if (verbose) {
    zsys_info("%s Removing Peer %6.6s", PREFIX, peerId.c_str());
  }

  for(DataMap::iterator it = subscribeData.begin(); it != subscribeData.end(); it++) {
    std::string key = it->first;
    Serializable* data = it->second;
    if (data) {
      data->removePeer(peerId);
      data->updated();
    }
  }
}
