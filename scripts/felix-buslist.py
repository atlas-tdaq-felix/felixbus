#!/usr/bin/env python3
"""
felix-buslist - Lists registrations on the FELIX Bus

    Usage:
      felix-buslist [options]

    Options:
      -e, --elinks                Print list by elinks
      -h, --help                  Display this help
      -i, --interface INTERFACE   Network interface to use [default: ZSYS_INTERFACE]
      -j, --json                  Print tables in json format (for -t)
      -m, --monitoring            Print the monitoring table.
      -s, --sleep TIME            Sleep in seconds [default: 2]
      -q, --quiet                 No big printouts
      -t, --tables                Print both tables (elinks, felix)
      -V, --version               Display version
      -v, --verbose               Run in verbose mode
      -z, --zyre-verbose          Run in Zyre verbose mode
"""
import time

from docopt import docopt  # noqa: E402

from libfelixbus_py import Bus, FelixTable, ElinkTable

if __name__ == "__main__":

    args = docopt(__doc__, version="0.1")

    felixTable = FelixTable()
    elinkTable = ElinkTable()
    monitTable = FelixTable()

    bus = Bus()
    bus.setInterface(args["--interface"])
    bus.setVerbose(args["--verbose"])
    bus.setZyreVerbose(args["--zyre-verbose"])

    bus.subscribe("FELIX", felixTable)
    bus.subscribe("ELINKS", elinkTable)
    if args["--monitoring"]:
        bus.subscribe("MONITORING", monitTable)

    bus.connect()

    print "Tables in FelixBus (ctrl\\ to quit)"

    counter = 0
    while True:
        counter += 1
        print "Update", counter
        if args["--elinks"]:
            print "Elink    Address"
            links = elinkTable.getLinks()
            for elink in links:
                felixId = elinkTable.getFelixId(elink)
                address = felixTable.getAddress(felixId)
                print elink, "    ", address
            print ""

        if args["--tables"]:
            # both tables
            if args["--json"]:
                o = {}
                o["felix"] = felixTable.toJson()
                o["elink"] = elinkTable.toJson()
                print "", o
            else:
                felixTable.print_table()
                print ""

            elinkTable.print_table()
            print ""

        if args["--monitoring"]:
            # the monitTable
            if args["--json"]:
                o = {}
                o["monitoring"] = monitTable.toJson()
                print "", o
            else:
                monitTable.print_table()
                print ""

        time.sleep(long(args["--sleep"]))

    exit(0)
