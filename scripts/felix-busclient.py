#!/usr/bin/env python3
"""
felix-busclient - Fakes registrations on the FELIX Bus.

    Usage:
      felix-client [options]

    Options:
      -e, --elinks NUMBER           Number of elinks to publish (ports are fake) [default: 10]
      -h, --help                    Display this help
      -g, --group-name GROUP_NAME   Group to use [default: FELIX]
      -i, --interface INTERFACE     Network interface to use [default: ZSYS_INTERFACE]
      -o, --offset NUMBER           Offset for elinks [default: 0]
      -s, --sleep TIME              Sleep in seconds before exitting [default: 2]
      -V, --version                 Display version
      -v, --verbose                 Run in verbose mode
      -z, --zyre-verbose            Run in Zyre verbose mode
"""
import time

from docopt import docopt  # noqa: E402

from libfelixbus_py import Bus, FelixTable, ElinkTable

if __name__ == "__main__":
    args = docopt(__doc__, version="0.1")

    felixTable = FelixTable()
    elinkTable = ElinkTable()
    monitTable = FelixTable()

    ip = Bus.getIpAddress()[1]
    print "Publishing Tables in FelixBus for", ip, "(ctrl\\ to quit)"

    address = "tcp://" + ip + ":53467"
    print "Address", address
    uuid = felixTable.addFelix(address)

    offset = long(args["--offset"])
    print "Elink offset", offset
    for elink in range(1, long(args["--elinks"])):
        elinkTable.addElink(offset + elink, uuid)

    bus = Bus()
    bus.setGroupName(args["--group-name"])
    bus.setInterface(args["--interface"])
    bus.setVerbose(args["--verbose"])
    bus.setZyreVerbose(args["--zyre-verbose"])

    bus.publish("FELIX", felixTable)
    bus.publish("ELINKS", elinkTable)

    bus.connect()

    sleepTime = long(args["--sleep"])
    print "Sleeping", sleepTime, "seconds."
    time.sleep(sleepTime)

    bus.disconnect()

    exit(0)
